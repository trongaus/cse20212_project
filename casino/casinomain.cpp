// Author: Madeline Gleason 
// Date: March 21, 2016
// main.cpp is the driver program for the casino base class

#include "casino.h"
#include "blackjack.h"
#include "roulette.h"
#include "holdem.h"
#include <iostream>
#include <iomanip>
using namespace std;

int main() {
	// instantiate objects of all derived classes
	casino C;
	holdem H;
//	blackjack B;
//	roulette R;
	
	// test calls to the various nested programs
	C.print();
	H.print();
//	B.print();
//	R.print();
	
	
	
	
/*	int bankrupt;
	bankrupt = C.isBankrupt();
	if (bankrupt==1){
		cout << "You are bankrupt!" << endl;
	}
*/
}
