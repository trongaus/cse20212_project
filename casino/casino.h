// Author: Madeline Gleason 
// Contributor: Reilly Kearney
// Final Project Casino Base Class

// This is the interface for the casino base class, which contains information regarding each game and the user's total money in the casino. For this stage in development casino is not yet a base class, since we have not created derived classes.

#ifndef casino_h
#define casino_h
#include <iostream>
#include <iomanip>
using namespace std;

class casino {
	public:
		casino(); 					// default constructor for casino class
		double calculateMoney();	// calculate net gain/loss after each game
		void print();				// print function
		int isBankrupt();	        // returns 1 if user is bankrupt, 0 otherwise
	private:
		double currentMoney;		// stores user's money throughout time in casino
		double initialMoney;		// stores user's initial monetary value
		string userName;			// stores user's name
		string gameName;			// stores name of game user is playing; update in derived classes	
		double gameMoney;			// stores the total amount won or loss in each game		
		double gameBet;				// stores bet user makes at beginning of each game
};

#endif 
