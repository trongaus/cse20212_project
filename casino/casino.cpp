// Author: Madeline Gleason
// Final Project Casino Base Class Functions

// This is the implementation of the casino base class.

#include <iostream>
#include <iomanip>
#include "casino.h"
using namespace std;

// casino default constructor
casino::casino() {
		currentMoney = 1000;					
		initialMoney = 1000;					
		userName = "mad dawg";	
		gameBet = 0;		
} 		

// calculate net gain/loss after each game
double casino::calculateMoney() {
		if (gameBet <= currentMoney){
			currentMoney = currentMoney - gameBet; 
			cout << "You now have $" << currentMoney << " to play with! Good luck!" << endl;
		}		
		else {
			cout << "You do not have enough money!" << endl;
		}
}

// pure virtual print function
void casino::print() {
	cout << "Welcome to The Circ, " << userName << "!" << endl;
	cout << "You currently have $" << currentMoney << " to play with!" << endl;
/*	cout << "Place a bet for your next game: " << endl;
	cin >> gameBet;
	calculateMoney();
*/
}

// returns 1 if user is bankrupt, 0 otherwise
int casino::isBankrupt() {

	double bankruptValue = 0;
	if (currentMoney <= bankruptValue) 
		return 1;
	else 
		return 0;

}
