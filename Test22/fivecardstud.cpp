// fivecardstud.cpp
// Author: Reilly Kearney
// class for composition in casino

#include <iostream>
#include <iomanip>
#include <string>
#include <deque>
#include <algorithm>
#include <stdlib.h>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include "fivecardstud.h"
using namespace std;

FiveCard::FiveCard() {
	
}

void FiveCard::initializeGame( double curr ) {
	pot = 0;

	/* Initialize Player */
	player.currentBet = 0;
	player.totalMoney = curr;
	player.handScore = 0;
	player.handType = "  ";

	/* Initialize Opponent */
	opponent.currentBet = 0;
	opponent.totalMoney = 10000;
	opponent.handScore = 0;
	opponent.handType = "  ";

	initializeDeck();
}

void FiveCard::initPrint() {
	// set the name of the file to print
	string file = "fivecardstud_word.txt";
	// print the file to stdout
	string tmp;
	ifstream myFile( file.c_str() );
	cout << endl;
	if ( myFile.is_open() ) {
		while( myFile.good() ) {
			getline( myFile, tmp );
			cout << tmp << endl;
		}
	}
}

void FiveCard::initializeDeck() {
	int rank = 0;
	Card card;
	for ( int j = 0; j < 13; j++ ) {
		for ( int i = 0; i < 4; i++ ) {
			card.suitRank  = i;
			card.number = j;
			card.valueRank = rank++;
			
			/* Suits */
			if      ( i == 0 ) { card.suit = 'C'; }
			else if ( i == 1 ) { card.suit = 'D'; }
			else if ( i == 2 ) { card.suit = 'H'; }
			else if ( i == 3 ) { card.suit = 'S'; }
			
			/* Value */
			if      ( j  <  8 ) { card.value = '0' + j + 2; }
			else if ( j ==  8 ) { card.value = 'T'; }
			else if ( j ==  9 ) { card.value = 'J'; }
			else if ( j == 10 ) { card.value = 'Q'; }
			else if ( j == 11 ) { card.value = 'K'; }
			else if ( j == 12 ) { card.value = 'A'; }

			/* Add card to deck */
			deck.push_back( card );
		}
	}
	shuffle();
}

void FiveCard::shuffle() {
	random_shuffle( deck.begin(), deck.end() );
}

double FiveCard::playFiveCard( double curr ) {
	int winner, t = 0, playerAction, opponentAction = 0;

	initializeGame( curr );

	while ( 1 ) {
		ante();
		deal();
		calculateHandScore();
		printHand( 0 );
		while ( 1 ) { 		//betting
			player.currentBet = 0;
			opponent.currentBet = 0;
			playerAction = betPrompt( opponentAction );
			
			if ( playerAction ) {
				cout << playerAction << endl;
				opponentAction = opponentBet( playerAction );
			}
			else { opponentAction = 4; }

			if ( ( playerAction == 0 ) || ( playerAction == 2 ) || ( opponentAction == 0 ) || ( opponentAction == 2 ) ) { break; }
			t++;
		}
		if ( playerAction && opponentAction ) {
			opponentAction = 0;
			draw();
			opponentDraw();
			calculateHandScore();
			printHand( 0 );
			sleep ( 2 );
			while ( 1 ) { 		//betting
				player.currentBet = 0;
				opponent.currentBet = 0;
				playerAction = betPrompt( opponentAction );
				
				if ( playerAction ) {
					opponentAction = opponentBet( playerAction );
				}
				else { opponentAction = 4; }
				
				if ( ( playerAction == 0 ) || ( playerAction == 2 ) || ( opponentAction == 0 ) || ( opponentAction == 2 ) ) { break; }
				t++;
			}
		}
		printHand( 1 );
		if ( !playerAction ) {
			cout << "You fold!!" << endl;
			winner = checkWinner( 0, 1);
		}
		else if ( !opponentAction ) {
			cout << "Opponent folds!!" << endl;
			winner = checkWinner( 1, 0 );
		}
		else {winner = checkWinner( 1, 1 ); }

		printHand( winner + 2 );

		if ( !playAgain() ) {
			return player.totalMoney;		// sends player back to casino
		}
		else if ( player.totalMoney < 30 ) {
			cout << "You don't have enough for the ante in! Try your luck elsewhere!\n";
			return player.totalMoney;		// sends player back to casino
		}
		reset();
		opponentAction = 0;
		playerAction = 0;
	}
}

void FiveCard::deal() {
	cout << "Dealing...\n\n";
	sleep( 2 );
	player.hand = new Card[ 5 ];
	opponent.hand = new Card[ 5 ];

	for ( int i = 0; i < 5; i++ ) {
		player.hand[ i ] = deck.front();
		deck.pop_front();					// delete card dealt
	}
	for ( int i = 0; i < 5; i++ ) {
		opponent.hand[ i ] = deck.front();
		deck.pop_front();					// delete card dealt
	}
}

void FiveCard::draw() {
	int choice, discard;
	cout << "Would you like to:\n(1) Stand Pat\t(2) Draw Cards\n";
	cin >> choice;

	if ( choice == 2 ) {
		cout << "How many cards would you like to draw: ";
		cin >> choice;
		if ( choice < 5 ) {
			for ( int i = 0; i < choice; i++ ) {
				cout << "Choose Card to Discard ( 1 - 5 ): ";
				cin >> discard;
				player.hand[ discard - 1 ] = deck.front();
				deck.pop_front();
			}
		}
		else {
			for ( int i = 0; i < 5; i++ ) {
				player.hand[ i ] = deck.front();
				deck.pop_front();
			}
		}
		cout << "Drawing Cards...\n";
		sleep( 2 );
	}
}

void FiveCard::opponentDraw() {
	for ( int i = 0; i < 5; i++ ) {
		if ( rand() % 2 ) {
			opponent.hand[ i ] = deck.front();
			deck.pop_front();
		}
	}
	cout << "Opponent drawing cards...\n";
	sleep( 2 );
}

void FiveCard::ante() {
	cout << "Ante Up!" << endl;
	player.totalMoney -= 30;
	opponent.totalMoney -= 30;
	pot += 60;
	sleep( 1 );
	cout << "Pot: " << pot << endl;
}

int FiveCard::betPrompt( int c ) {
	int choice, bet;
	
	cout << "Your Money: $" << player.totalMoney << endl;

	if ( !c ) { 
		cout << "Would you like to:\n(1) Fold\t(2) Check\t(3) Bet/Call\n";
		cin >> choice;
	}
	else {
		cout << "Would you like to:\n(1) Fold\t(3) Call\n";
		cin >> choice;
	}

	switch ( choice ) {
		case 1:
			return 0;
		case 2:
			player.currentBet = 0;
			return 1;
		case 3:
			if ( c ) {
				if ( opponent.currentBet > player.totalMoney ) { player.currentBet = player.totalMoney; }
				else { player.currentBet = opponent.currentBet; }
			}
			else {
				cout << "Enter your bet: $";
				cin >> bet;
				while ( bet > player.totalMoney ) {
					cout << "You don't have enough money! Bet again.\n"; 
					cout << "Enter your bet: $";
					cin >> bet;
				}
				//player.currentBet = opponent.currentBet + bet;
				player.currentBet = bet;
			}
			player.totalMoney -= player.currentBet;
			pot += player.currentBet;
			break;
	}
	if ( player.currentBet == opponent.currentBet ) { return 2; }
	else if ( player.currentBet == 0 ) { return 1; }
	else { return 3; }
}

int FiveCard::opponentBet( int c ) {
	if ( c == 1 ) {
		if ( opponent.handScore > 100000000 ) {
			opponent.currentBet = rand( ) % 201;
			if ( opponent.currentBet < 150 ) { opponent.currentBet += 150; }
		}
		else if ( opponent.handScore > 1000000 ) {
			opponent.currentBet = rand( ) % 151;
			if ( opponent.currentBet < 100 ) { opponent.currentBet += 100; }
		}
		else if ( opponent.handScore > 10000 ) {
			opponent.currentBet = rand( ) % 101;
			if ( opponent.currentBet < 50 ) { opponent.currentBet += 50; }
		}
		else if ( opponent.handScore > 100 ) {
			opponent.currentBet = rand( ) %  51;
			if ( opponent.currentBet < 15 ) { opponent.currentBet += 15; }
		}
		else {
			opponent.currentBet = 0;
		}
	}
	else if ( c == 2 ) {
		return 1;
	}
	else if ( c == 3 ) {
		if ( opponent.handScore > 44 ) { opponent.currentBet = player.currentBet; }
		else { return 0; }
	}
	
	if ( opponent.currentBet == 0 ) { cout << "Opponent checks\n"; }
	else { cout << "Opponent bets $" << opponent.currentBet << endl; }
	
	opponent.totalMoney -= opponent.currentBet;
	pot += opponent.currentBet;
	
	if ( opponent.currentBet == player.currentBet ) { return 2; }
	else if ( opponent.currentBet == 0 ) { return 1; }
	else { return 3; }
}

int FiveCard::playAgain() {
	char c;
	cout << "\nYou now have: $" << player.totalMoney << endl;
	sleep( 1 );
	cout << "Would you like to play again? (Y/N) ";
	cin >> c;

	if ( c == 'Y' || c == 'y' ) { return 1; }
	else if ( c == 'N' || c == 'n' ) { return 0; }
	else {
		cout << "Invalid input." << endl;
		return playAgain();
	}
}

int FiveCard::checkWinner( int playerStatus, int opponentStatus) {
	cout << "\nDetermining winner...\n";
	sleep ( 1 );
	
	calculateHandScore();
	
	if ( !playerStatus ) {
		opponent.totalMoney += pot;
		return 1;
	}
	else if ( !opponentStatus ) {
		player.totalMoney += pot;
		return 0;
	}
	else if ( player.handScore > opponent.handScore ) {			// player wins
		player.totalMoney += pot;
		return 0;
	}	
	else if ( player.handScore < opponent.handScore ) {		// opponent wins
		opponent.totalMoney += pot;
		return 1;
	}	
	else {
		player.totalMoney   += pot/2;
		opponent.totalMoney += pot/2;
		return 2;	// for compiling
	}	
}

void FiveCard::calculateHandScore() {
	player.handScore = 0;
	opponent.handScore = 0;
	/* Player */
	if ( checkStraight( 0 ) && checkFlush( 0 ) ) {
		player.handType =  "Straight Flush";
		player.handScore *= 10;
	}
	else if ( checkFourofaKind( 0 ) ) { player.handType = "Four of a Kind"; }
	else if ( checkFullHouse( 0 ) ) { player.handType = "Full House"; }
	else if ( checkFlush( 0 ) ) { player.handType = "Flush"; }
	else if ( checkStraight( 0 ) ) { player.handType = "Straight"; }
	else if ( checkThreeofaKind( 0 ) ) { player.handType = "Three of a Kind"; }
	else if ( checkTwoPair( 0 ) ) { player.handType = "Two Pair"; }
	else if ( checkOnePair( 0 ) ) { player.handType = "One Pair"; }
	else if ( checkHighCard( 0 ) ) { player.handType = "High Card"; }
	
	/* Opponent */
	if ( checkStraight( 1 ) && checkFlush( 1 ) ) {
		opponent.handType =  "Straight Flush";
		opponent.handScore *= 10;
	}
	else if ( checkFourofaKind( 1 ) ) { opponent.handType = "Four of a Kind"; }
	else if ( checkFullHouse( 1 ) ) { opponent.handType = "Full House"; }
	else if ( checkFlush( 1 ) ) { opponent.handType = "Flush"; }
	else if ( checkStraight( 1 ) ) { opponent.handType = "Straight"; }
	else if ( checkThreeofaKind( 1 ) ) { opponent.handType = "Three of a Kind"; }
	else if ( checkTwoPair( 1 ) ) { opponent.handType = "Two Pair"; }
	else if ( checkOnePair( 1 ) ) { opponent.handType = "One Pair"; }
	else if ( checkHighCard( 1 ) ) { opponent.handType = "High Card"; }
}

int FiveCard::checkFourofaKind( int x ) {
	int count = 0, score = 0;
	char match;
	if ( !x ) {			// player
		for ( int j = 0; j < 2; j++ ) {
			match = player.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 4 ) {
				player.handScore = score*100000000;
				return 1;
			}
			count = 0;
		}
	}
	else if ( x ) {		// opponent
		for ( int j = 0; j < 2; j++ ) {	
			match = opponent.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 4 ) {
				opponent.handScore = score*100000000;
				return 1;
			}
			count = 0;
		}
	}
	return 0;
}

int FiveCard::checkFullHouse( int x ) {
	/* Check Three of a Kind */
	int count = 0, score = 0;
	char match, previous;
	int check = 0;
	if ( !x ) {			// player
		for ( int j = 0; j < 3; j++ ) {
			match = player.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 3 ) {
				player.handScore += score;
				check = 1;
				previous = match;
				break;
			}
			count = 0;
		}
	}
	else if ( x ) {		// opponent
		for ( int j = 0; j < 3; j++ ) {
			match = opponent.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 3 ) {
				opponent.handScore += score;
				check = 1;
				previous = match;
				break;
			}
			count = 0;
		}
	}

	/* Check Pair */
	count = 0;
	score = 0;
	if ( !x && check ) {	// player
		check = 0;
		for ( int j = 0; j < 4; j++ ) {
			match = player.hand[ j ].value;
			if ( match == previous ) { continue; }
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				player.handScore += score;
				check = 1;
				break;
			}
			count = 0;
		}
	}
	else if ( x && check ) {	// opponent
		check = 0;
		for ( int j = 0; j < 4; j++ ) {
			match = opponent.hand[ j ].value;
			if ( match == previous ) { continue; }
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				opponent.handScore += score;
				check = 1;
				break;
			}
			count = 0;
		}
	}

	if ( check ) {
		if ( !x ) { player.handScore *= 10000000; }
		else if ( x )  { opponent.handScore *= 10000000; }
	}
	else {
		if ( !x ) { player.handScore = 0; }
		else if ( x )  { opponent.handScore = 0; }
	}

	return check;
}

int FiveCard::checkFlush( int x ) {
	if ( !x ) {			// player
		for ( int i = 1; i < 5; i++ ) {
			if ( player.hand[ 0 ].suitRank != player.hand[ i ].suitRank ) { return 0; }
		}
		for ( int i = 0; i < 5; i++ ) {
			player.handScore += player.hand[ i ].valueRank;
		}
		player.handScore *= 1000000;
	}
	else if ( x ) {		// opponent
		for ( int i = 1; i < 5; i++ ) {
			if ( opponent.hand[ 0 ].suitRank != opponent.hand[ i ].suitRank ) { return 0; }
		}
		for ( int i = 0; i < 5; i++ ) {
			opponent.handScore += opponent.hand[ i ].valueRank;
		}
		opponent.handScore *= 1000000;
	}
	return 1;
}

int FiveCard::checkStraight( int x ) {
	int values[ 5 ], score;
	int check = 1;
	if ( !x ) {			// player
		for ( int i = 0; i < 5; i++ ) {
			values[ i ] = player.hand[ i ].number;
		}
	}
	else if ( x ) {		// opponent
		for ( int i = 0; i < 5; i++ ) {
			values[ i ] = opponent.hand[ i ].number;
		}
	}

	sort( values, values + 5 );
	
	for ( int i = 0; i < 4; i++ ) {
		if ( values[ i ] != ( values[ i + 1 ] - 1 ) ) { check = 0; }
	}
	
	if ( !check && ( find( values, values + 5, 12 ) != ( values + 5 ) ) ) {
		replace( values, values + 5, 12, -1 );
		sort( values, values + 5 );
		check = 1;
		for ( int i = 0; i < 4; i++ ) {
			if ( values[ i ] != ( values[ i + 1 ] - 1 ) ) { check = 0; }
		}
	}

	if ( check ) {
		for ( int i = 0; i < 5; i++ ) { score += values[ i ]; }
		if ( !x ) { player.handScore += score*100000; }
		else if ( x ) { opponent.handScore += score*100000; }
	}
	
	return check;
}

int FiveCard::checkThreeofaKind( int x ) {
	int count = 0, score = 0;
	char match;
	if ( !x ) {			// player
		for ( int j = 0; j < 3; j++ ) {
			match = player.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 3 ) {
				player.handScore = score*100000;
				return 1;
			}
			count = 0;
		}
	}
	else if ( x ) {		// opponent
		for ( int j = 0; j < 3; j++ ) {
			match = opponent.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 3 ) {
				opponent.handScore = score*100000;
				return 1;
			}
			count = 0;
		}
	}
	return 0;
}

int FiveCard::checkTwoPair( int x ) {
	int count = 0, score = 0, pairs = 0;
	char match, previous = 'g';
	if ( !x ) {			// player
		for ( int j = 0; j < 4; j++ ) {
			match = player.hand[ j ].value;
			if ( match == previous ) { continue; }
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				pairs++;
				if ( pairs == 2 ) {
					player.handScore = score*1000;
					return 1;
				}
				previous = match;
			}
			count = 0;
		}
	}
	else if ( x ) {		// opponent
		for ( int j = 0; j < 4; j++ ) {
			match = opponent.hand[ j ].value;
			if ( match == previous ) { continue; }
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				pairs++;
				if ( pairs == 2 ) {
					opponent.handScore = score*1000;
					return 1;
				}
				previous = match;
			}
			count = 0;
		}
	}
	return 0;
}

int FiveCard::checkOnePair( int x ) {
	int count = 0, score = 0;
	char match;
	if ( !x ) {			// player
		for ( int j = 0; j < 4; j++ ) {
			match = player.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == player.hand[ i ].value ) {
					count++;
					score += player.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				player.handScore = score*100;
				return 1;
			}
			count = 0;
			score = 0;
		}
	}
	else if ( x ) {		// opponent
		for ( int j = 0; j < 4; j++ ) {
			match = opponent.hand[ j ].value;
			for ( int i = 0; i < 5; i++ ) {
				if ( match == opponent.hand[ i ].value ) {
					count++;
					score += opponent.hand[ i ].valueRank;
				}
			}
			if ( count == 2 ) {
				opponent.handScore = score*100;
				return 1;
			}
			count = 0;
			score = 0;
		}
	}
	return 0;
}

int FiveCard::checkHighCard( int x ) {
	int high = 0;
	if ( !x ) {
		for ( int i = 0; i < 5; i++ ) {
			if ( player.hand[ i ].valueRank > high ) { high = player.hand[ i ].valueRank; }
		}
		player.handScore = high;
	}
	else if ( x ) {
		for ( int i = 0; i < 5; i++ ) {
			if ( opponent.hand[ i ].valueRank > high ) { high = opponent.hand[ i ].valueRank; }
		}
		opponent.handScore = high;
	}
	return 1;
}

void FiveCard::reset() {
	cout << "\nReshuffling...\n";
	sleep ( 2 );
	initializeDeck();
	pot = 0;
	delete[] player.hand;
	delete[] opponent.hand;
	player.handScore = 0;
	opponent.handScore = 0;
}

void FiveCard::printHand( int n ) {
	if ( n == 2 ) {
		cout << "\nYou win!!\t" << player.handType << endl;
		n -= 2;
	}
	else if ( n == 3 ) {
		cout << "\nOpponent Wins.\t" << opponent.handType << endl;
		n -= 2;
	}
	else if ( n == 0 ) { cout << "\nYour Hand:\t" << player.handType << endl; }
	else if ( n == 1 ) { cout << "\nOpponent Hand:\t" << opponent.handType << endl; }
	
	switch ( n ) {
		cout << endl;
		case 0:		// print just player hand
			cout << "  ---   ---   ---   ---   --- \n";
			cout << " | " << player.hand[ 0 ].value;
			cout << " | | " << player.hand[ 1 ].value << " | | " << player.hand[ 2 ].value;
			cout << " | | " << player.hand[ 3 ].value << " | | " << player.hand[ 4 ].value << " | " << endl;
			cout << " | " << player.hand[ 0 ].suit;
			cout << " | | " << player.hand[ 1 ].suit << " | | " << player.hand[ 2 ].suit;
			cout << " | | " << player.hand[ 3 ].suit << " | | " << player.hand[ 4 ].suit << " | " << endl;
			cout << "  ---   ---   ---   ---   --- \n";
			cout << "  (1)   (2)   (3)   (4)   (5) \n";
			break;
		case 1:		// print opponent hand
			cout << "  ---   ---   ---   ---   --- \n";
			cout << " | " << opponent.hand[ 0 ].value;
			cout << " | | " << opponent.hand[ 1 ].value << " | | " << opponent.hand[ 2 ].value;
			cout << " | | " << opponent.hand[ 3 ].value << " | | " << opponent.hand[ 4 ].value << " | " << endl;
			cout << " | " << opponent.hand[ 0 ].suit;
			cout << " | | " << opponent.hand[ 1 ].suit  << " | | " << opponent.hand[ 2 ].suit;
			cout << " | | " << opponent.hand[ 3 ].suit  << " | | " << opponent.hand[ 4 ].suit  << " | " << endl;
			cout << "  ---   ---   ---   ---   --- \n";
			break;
	}
}
