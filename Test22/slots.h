// slots.h (class definition)
// Due 2/8/16
// Author: Ronni Sardina
// Derived class of casino

#ifndef slots_H
#define slots_H
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <time.h>
#include <set>
#include <map>
#include "math.h"
#include "casino.h"
using namespace std;

// Derived slots class; inherits from casino
class slots {
	public:
		slots();	// default constructor for slots class
		double playSlots(double);	
		void print(double);
		void initPrint();
		int again();
	private:
		char spin();
		int compare();
		char wheel1;
		char wheel2;
		char wheel3;
		char wheel4;
		int matches;
		//char images[9] = {'☘', '☂', '☮', '✱', '✈', '♥', '♦', '♠', '♣'};
		char images[9] = {'X', 'O', '#', '%', '$', '@', '*', '?', '+'};
		map<char, int> iconCount;
		int play;
		double gameBet;				// stores bet user makes at beginning of each game
		//double gameMoney;			// stores the total amount won or loss in each game
};

#endif
