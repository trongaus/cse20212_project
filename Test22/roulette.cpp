// roulette.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Derived Class of casino

#include "casino.h"
#include "roulette.h"

using namespace std;

// default constructor for the roulette class
roulette::roulette() {
}

// return random number the ball lands on
int roulette::randomNum() {
	srand((int)time(0));
	int r = (rand() % 37);
	return r;
}

void roulette::initPrint() {
	// set the name of the file to print
	string file = "roulette_word.txt";
	// print the file to stdout
	string tmp;
	ifstream myFile (file.c_str());
	cout << endl;
	if (myFile.is_open()) {
		while(myFile.good()) {
			getline(myFile, tmp);
			cout << tmp << endl;
		}
	}
}

// determine if the user wants to play/ bet again
int roulette::again() {
	char c = 'Z';
	if (c != 'Y' || c != 'N' || c != 'y' || c != 'n'){
		cin >> c;
	} 
	if (c == 'Y' || c == 'y') {
		return 1;
	}
	else {
		return 0;
	}
}

// function to play the game
double roulette::playRoulette(double money) {
	
	// initialize "global" variables
	int payouts[9] = {0, 35, 3, 3, 3, 2, 2, 2, 2};	// matching payouts for 1-10
	int playGame = 1;				// determine if the user would like to play again or not
	int RN;							// initialize variable for random number the ball lands on
	cout << "Your initial balance is $" << money << endl;
	
	while (playGame == 1) {
		// initialize local variables-- need to be reset each time through while loop
		int done = 0;				// 0 or 1 to determine if user is done placing chips/ bets
		int i = 0;					// loop counters
		int another = 0;			// 0 or 1 to determine if user wants to place another bet
		int choice[10] = {0};	// array of up to 10 choices for the user's bet
		double number[10];			// array of up to 10 choices for the individual number bet
		int win[10] = {0};		// to be updated w/ corresponding payouts for winning bets
		double userBet[10] = {0};	// initialize bet amount
	
		// run while loop while user continues to place bets
		while (!done) {
			
			// determine the actual value the user wants to bet
			while (userBet[i] <= 0 || userBet[i] > money) {
				cout << "How much would you like to bet on your chip? $";
				cin >> userBet[i];
				if (userBet[i] <= 0 || userBet[i] > money) {
					cout << "Please enter a bet greater than zero and less than your current wallet." << endl;
				}
			}
			money = money - userBet[i];
			
			// print the table
			cout << "\nTHE TABLE: " << endl;
			 cout << "__________________________________________" << endl
			 <<"0 |1  4  7  10 | 13 16 19 22 | 25 28 31 34|"<<endl 
			  <<"--|2  5  8  11 | 14 17 20 23 | 26 29 32 35|"<<endl 
			  <<"  |3  6  9  12 | 15 18 21 24 | 27 30 33 36|"<<endl 
			  <<"  |    1st12   |     2nd12   |   3rd12    |       "<<endl
			  <<"  |  1stHalf   | Even | Odd  |   2ndHalf  |          "<<endl
			  << "__________________________________________" << endl << endl;
			sleep(2);
			// print the profit margin
			 cout<<"THE PROFIT MARGIN:"<<endl 
			  <<"+---------------+"<<endl 
			  <<"|Even    |  2X  |"<<endl 
			  <<"|Odd     |  2X  |"<<endl 
			  <<"|Halves  |  2X  |"<<endl 
			  <<"|12ths   |  3X  |"<<endl 
			  <<"|Numbers | 35X  |"<<endl 
			  <<"+---------------+"<<endl<<endl; 
			sleep(2);
			// print the betting options
			cout << "YOUR OPTIONS:" << endl;
			cout << "     1) an individual number: 0, 1, 2 ... 36" << endl;
			cout << "     2) the first 12 numbers" << endl;
			cout << "     3) the second 12 numbers" << endl;
			cout << "     4) the third 12 numbers" << endl;
			cout << "     5) numbers 1 to 18" << endl;
			cout << "     6) numbers 19 to 36" << endl;
			cout << "     7) even numbers" << endl;
			cout << "     8) odd numbers" << endl << endl;
			sleep(2);
			// determine which number above the user chose
			while (choice[i] < 1 || choice[i] > 8) {
				cout << "What option would you like to place this bet on? ";
				cin >> choice[i];
				if (choice[i] < 1 || choice[i] > 8) {
					cout << "Error: please enter a valid choice 1-8. " << endl;
				}
			}
			
			// if choice 1 is chosen, determine the number the user would like to bet on
			if (choice[i] == 1) {
				cout << "Which number 0 - 36 would you like to bet on? ";
				cin >> number[i];
				cout << number[i] << endl;
			}
			
			// determine if the user wants to place another bet on this round
			if (money > 0) {
				cout << "You still have $" << money << endl;
				cout << "Would you like to place another bet? (Y/N) "; 
				another = again();
			}
					
			// checks to see if done placing bets
			if (another != 1) {
				done = 1;
			}
			if (i == 9) {
				cout << "Sorry, you can't place more than 10 bets on this round." << endl;
				done = 1;
			}
			i++;
		}

		// determine the result of the spin using random number generation
		RN = randomNum();
		cout << "\nThe ball is spinning..." << endl;
		sleep(1);
		cout << "The number spun was " << RN << endl << endl;
		sleep(1);
			
		// check the various user bets to determine if their bet matches the random number condition and update the wins array to the corresponding payout
		for (int j=0; j<8; j++) {
		
			// RN matches number exactly
			if (choice[j] == 1) {
				if (number[j] == RN) {
					win[j] = payouts[1];
				}
				else {
					win[j] = -1 * payouts[1];
				}
			}
			
			// RN matches #s 1-12
			else if (choice[j] == 2) {
				if ((RN >= 1) && (RN <= 12)) {
					win[j] = payouts[2];
				}
				else {
					win[j] = -1 * payouts[2];
				}
			}
			
			// RN matches #s 13-24
			else if  (choice[j] == 3) {
				if ((RN >= 13) && (RN <= 24)) {
					win[j] = payouts[3];
				}
				else {
					win[j] = -1 * payouts[3];
				}
			}
			
			// RN matches #s 25-36
			else if (choice[j] == 4) {
				if ((RN >= 25) && (RN <= 36)) {
					win[j] = payouts[4];
				}
				else {
					win[j] = -1 * payouts[4];
				}
			}
			
			// RN matches #s 1-18
			else if (choice[j] == 5) {
				if ((RN >= 1) && (RN <= 18)) {
					win[j] = payouts[5];
				}
				else {
					win[j] = -1 * payouts[5];
				}
			}
			
			// RN matches #s 19-36
			else if (choice[j] == 6) {
				if ((RN >= 19) && (RN <= 36)) {
					win[j] = payouts[6];
				}
				else {
					win[j] = -1 * payouts[6];
				}
			}
			
			// RN matches even #s
			else if (choice[j] == 7) {
				if (RN % 2 == 0) {
					win[j] = payouts[7];
				} 
				else {
					win[j] = -1 * payouts[7];
				}
			}
			
			// RN matches odd #s
			else if (choice[j] == 8) {
				if (RN % 2 != 0) {
					win[j] = payouts[8];
				} 
				else {
					win[j] = -1 * payouts[8];
				}
			}
		}
	
		// Determine if the user's bets won or lost & indicate amount won or lost
		for (int k=0; k<10; k++) {
			if (win[k] > 0) {
				cout << "Congrats! One of your bets won! You will receive $" << win[k]*userBet[k] << endl;
				money = money + win[k]*userBet[k];
			}
			else if (win[k] < 0) {
				cout << "So sorry. One of your bets lost." << endl;
			}
		}
		cout << endl;
		cout << "Your current balance is $" << money << endl;
		if (money > 0) {
			cout << "Would you like to play again? (Y/N) "; 
			playGame = again();
		}
		else {
			return money;
		}
		cout << endl;
	}
	return money;
}
