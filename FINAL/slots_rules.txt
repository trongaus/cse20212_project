Slots Rules
============

The concept of the slot machine is incredibly simple. All you do is grab a slot machine, put your money in, and push the button. The reels will spin and then slowly come to a rest. If the symbols on those reels line up to produce a winning combination, you win the number of credits shown on the slot’s pay table.

For the purposes of this program, all that you need to do to run it is select the game from the main menu- the program will do the rest for you. You will automatically be deducted $10 for your initial bet. If none of the icons on the machine match, you receive no payout. If you match two icons, you will receive $5, if you match three icons, you will receive $30, and if you match all four icons, you will receive $500.

After the machine has run, you have the opportunity to select Y/N for if you would like to play the game again. Selecting Y will deduct your $10 initial bet once again, and selecting N will return you to the main casino floor.
