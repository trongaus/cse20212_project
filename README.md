# README #

Madeline Gleason, Reilly Kearney, Taylor Rongaus, Veronica Sardina

This repository is devoted to our CSE20212 final project. We will be creating a "Casino" with various related games that a user will be able to play.

The final submission of the project can be viewed in the "FINAL" folder within the repository.