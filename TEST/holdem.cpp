// holdem.cpp
// Due 2/8/16
// Author: Taylor Rongaus
// Derived Class of casino

#include "casino.h"
#include "holdem.h"

using namespace std;

// default constructor for the holdem class
holdem::holdem() : casino() {
}

// derived class function calls
 
void holdem::initPrint() {
	// set the name of the file to print
	string file = "holdem_word.txt";
	// print the file to stdout
	string tmp;
	ifstream myFile (file.c_str());
	cout << endl;
	if (myFile.is_open()) {
		while(myFile.good()) {
			getline(myFile, tmp);
			cout << tmp << endl;
		}
	}
}
