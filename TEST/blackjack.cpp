// blackjack.cpp
// Due 2/8/16
// Author: Reilly Kearney and Taylor Rongaus
// Derived Class of casino

#include "casino.h"
#include "blackjack.h"

using namespace std;

// default constructor for the blackjack class
blackjack::blackjack() : casino() {
}

blackjack::blackjack( double curr, int n ) {
	/* Initialize Player */
	player.currentBet = 0;
	player.totalMoney = curr;
	player.handScore  = 0;
	startMoney        = curr;
	currentMoney      = curr;

	/* Initialize Dealer */
	opponent.currentBet = 0;
	opponent.totalMoney = 0;
	opponent.handScore  = 0;

	for ( int i = 0; i < n; i++ ) {
		deck.push_back( i % 13 );		// creates deck with values from 0 - 12
	}
	shuffle();
}

void blackjack::initPrint() {
	// set the name of the file to print
	string file = "blackjack_word.txt";
	// print the file to stdout
	string tmp;
	ifstream myFile ( file.c_str() );
	cout << endl;
	if ( myFile.is_open() ) {
		while( myFile.good() ) {
			getline( myFile, tmp );
			cout << tmp << endl;
		}
	}
}

void blackjack::initializeDeck() {
	int rank = 0;
	Card card;
	for ( int j = 0; j < 13; j++ ) {
		for ( int i = 0; i < 4; i++ ) {
			card.suitRank  = i;
			card.number = j;
			if ( j > 8 ) { card.valueRank = 10; }
			else { card.valueRank = j + 1; }
			
			/* Suits */
			if      ( i == 0 ) { card.suit = 'C'; }
			else if ( i == 1 ) { card.suit = 'D'; }
			else if ( i == 2 ) { card.suit = 'H'; }
			else if ( i == 3 ) { card.suit = 'S'; }
			
			/* Value */
			if      ( j ==  0 ) { card.value = 'A' }
			else if	( j <=  8 ) { card.value = '0' + j + 1; }
			else if ( j ==  9 ) { card.value = 'T'; }
			else if ( j == 10 ) { card.value = 'J'; }
			else if ( j == 11 ) { card.value = 'Q'; }
			else if ( j == 12 ) { card.value = 'K'; }

			/* Add card to deck */
			deck.push_back( card );
		}
	}
	shuffle();
}

void blackjack::shuffle() {
	random_shuffle( deck.begin(), deck.end() );
}

int blackjack::getSize() {
	return deck.size();
}

void blackjack::playBlackJack() {
	int dealerWins = 0, playerWins = 0, push = 0;	// total wins for both dealer & player
	int playOn = 1;									// 1 for play game, 0 for stop
	int continueHand = 1;							// 1 for keep asking player, 0 for move onto dealer	
	int playerHand = 0, dealerHand = 0;				// Hand for both player & dealer
	int check, winner;

	while ( playOn ) {
		/* Print Money */
		cout << "Your Current Money: " << currentMoney << endl;

		/* Place Bet */
		cout << "Place your bet: ";
		cin >> player.currentBet;
		if ( !placeBet( player.currentBet ) ) { continue; }

		/* Deal Initial Cards */
		Deal();
		
		/* Check for Player BlackJack */
		if ( check21( playerHand, 'P' ) ) {
			continueHand = 0;
		}

		/* Player Turn */
		while ( continueHand ) { 
			check = checkHit();
			if ( check ) {
				playerHand += checkCardValue( deck.front() );
				cout << "You Received: ";
				printCard( deck.front() );
				deck.pop_front();
			}
			if ( ( playerHand > 21 ) || ( !check ) ) {
				continueHand = 0;
			}
			cout << "Current Hand: " << playerHand << endl;
		}
		
		cout << endl;
		
		/* Check for Dealer BlackJack */
		check21( dealerHand, 'D' );
		
		cout << "Dealer has " << dealerHand << endl;

		/* Dealer Turn */
		while ( dealerHand < 17 ) {
			dealerHand += checkCardValue( deck.front() );
			cout << "Dealer received ";
			printCard( deck.front() );
			deck.pop_front();
			cout << "Dealer now has " << dealerHand << endl;
		}

		/* Check for Winner */
		winner = checkWinner();
		switch ( winner ) {
			case 0:	// player loses
				dealerWins += 1;
				break;
			case 1:	// player wins
				playerWins += 1;
				break;
			case 2:	// push
				push += 1;
				break;
		}
	
		/* Print Money */
		cout << "Your Current Money: " << currentMoney << endl;

		/* Prompt for Continue Play */
		playOn = playAgain();

		/* Check Deck Size */
		if ( getSize() <= 15 ) { initializeDeck(); }

		/* Reset GamePlay */
		delete[] player.hand;
		delete[] dealer.hand;
		player.currentBet = 0;
		continueHand = 1;
	}
	
	/* Display Stats */
	cout << endl;
	cout << "Player Won " << playerWins << " Games." << endl;
	cout << "Dealer Won " << dealerWins << " Games." << endl;
	cout << "Pushed " << push << " Games." << endl;
	cout << endl;
}

int blackjack::placeBet( double bet ) {
	currentBet = bet;
	if (currentBet > currentMoney) { return 0; }
	else {
		currentMoney = currentMoney - player.currentBet;
		return 1;
	}
}

void blackjack::Payout( int n ) {
	switch (n) {
		case 0:
			break;
		case 1: // user won
			currentMoney = currentMoney + (2 * player.currentBet);	// paid out
			break;
		case 2: // pushed
			currentMoney = currentMoney + player.currentBet;		// break even
			break;
		case 3: // blackjack
			currentMoney = currentMoney + (4 * player.currentBet);	// paid out double
	}
}

void blackjack::Deal() {
	cout << "Dealing...\n";
	player.hand = new Card[ 2 ];
	dealer.hand = new Card[ 2 ];

	cout << "\nYour Hand: " << endl;
	for ( int i = 0; i < 2; ++i ) {
		player.hand[ i ] = deck.front();
		deck.pop_front();				// delete card dealt
	}
	cout << "\nDealer's Hand: " << endl;
	cout << "The dealer has one card facedown" << endl;
	for ( int i = 0; i < 2; i++ ) {
		dealer.hand[ i ] = deck.front();
		deck.pop_front();				// delete card dealt
	}
}

int blackjack::playAgain() {
	char c;
	cout << "Would you like to play again? (Y/N) ";
	cin >> c;

	if (c == 'Y' || c == 'y') {
		return 1;
	}
	else if (c == 'N' || c == 'n') {
		return 0;
	}

	return 0;
}

int blackjack::checkWinner () {
	cout << endl;
	if ( playerHand <= 21 ) {
		if ( dealerHand > 21 ) {
			cout << "Congrats! Dealer Bust! You Win!" << endl;
			Payout(1);
			return 1;
		}
		else if ( playerHand > dealerHand ) {
			cout << "Congrats! You Win!" << endl;
			Payout(1);
			return 1;
		}
		else if ( playerHand < dealerHand ) {
			cout << "Dealer Wins! Better Luck next time!" << endl;
			Payout(0);
			return 0;
		}
		else if ( playerHand == dealerHand ) {
			cout << "You Pushed!" << endl;
			Payout(2);
			return 2;
		}
	}
	else if ( playerHand > 21 ) {
		cout << "You Bust! Dealer Wins! Better Luck next time!" << endl;
		return 0;
	}

	return 0;
}

int blackjack::checkHit() {
	char c;
	cout << "\nWould you like to hit? (Y/N) ";
	cin >> c;
	if ( c == 'Y' || c == 'y' ) {
		return 1;
	}
	else if ( c == 'N' || c == 'n' ) {
		return 0;
	}
	else {
		return 0;
	}
}

int blackjack::checkCardValue( int v ) {
	int output;
	switch ( v ) {
		case 1:		// Ace
			output = 11;
			break;
		case 11:	// Jack
		case 12:	// Queen
		case 0:		// King
			output = 10;
			break;
		default:
			output = v;
	}
	return output;
}

void blackjack::printCard( int v ) {
	switch (v) {
		case 1:
			cout << "Ace " << endl;
			break;
		case 11:
			cout << "Jack " << endl;
			break;
		case 12:
			cout << "Queen " << endl;
			break;
		case 0:
			cout << "King " << endl;
			break;
		default:
			cout << v << " " << endl;
	}
}

int blackjack::check21( int hand, char z ) {
	if (hand == 21 && z == 'P') {
		cout << "YOU GOT BLACKJACK!" << endl;
		Payout(3);
		return 1;
	}
	else if (hand == 21 && z == 'D') {
		cout << "DEALER GOT BLACKJACK!" << endl;
		Payout(0);
		return 1;
	}
	else {
		return 0;
	}
}

double blackjack::netChange() {
	double netChange;
	netChange = currentMoney - startMoney;
	return netChange;
}

void FiveCard::printHand( int n ) {
	if ( n == 2 ) {
		cout << "\nYou win!!\t" << player.handType << endl;
		n -= 2;
	}
	else if ( n == 3 ) {
		cout << "\nHouse Wins.\t" << opponent.handType << endl;
		n -= 2;
	}
	else if ( n == 0 ) { cout << "\nYour Hand:\t"   << player.handType << endl; }
	else if ( n == 1 ) { cout << "\nDealer Hand:\t" << dealer.handType << endl; }
	
	switch ( n ) {
		cout << endl;
		case 0:		// print just player hand
			cout << "  ---   --- \n";
			cout << " | " << player.hand[ 0 ].value  << " | | " << player.hand[ 1 ].value << " | |\n";
			cout << " | " << player.hand[ 0 ].suit   << " | | " << player.hand[ 1 ].suit  << " | |\n";
			cout << "  ---   --- \n";
			break;
		case 1:		// print dealer hand
			cout << "  ---   --- \n";
			cout << " | " << dealer.hand[ 0 ].value  << " | | " << dealer.hand[ 1 ].value << " | |\n";
			cout << " | " << dealer.hand[ 0 ].suit   << " | | " << dealer.hand[ 1 ].suit  << " | |\n";
			cout << "  ---   --- \n";
			break;
	}
}


