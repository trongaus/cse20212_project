/*	Author: 		Taylor Rongaus			*
 *	Date:			03/23/2015				*
 *	Description:	Roulette Driver File for final project			*/

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include "roulette.cpp"

using namespace std;

int main() {
	Roulette r;						// instantiate object of roulette class
	r.playRoulette();
}
