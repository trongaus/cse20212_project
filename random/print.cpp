// print.cpp
// Author: Taylor Rongaus
// file to print cool text file images to the screen

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
using namespace std;

int main() {
	// read in the name of the files
	string file;
	cout << "What is the name of the file? ";
	cin >> file;
	// print the file to stdout
	string tmp;
	ifstream myFile (file.c_str());
	if (myFile.is_open()) {
		while(myFile.good()) {
			getline(myFile, tmp);
			cout << tmp << endl;
		}
	}
}
