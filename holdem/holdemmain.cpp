/*	Author: 		Reilly Kearney									*
 *	Date:			03/23/2015										*
 *	Description:	Hold 'em Driver File for final project			*/

#include <iostream>
#include <iomanip>
#include <string>
#include <deque>
#include <algorithm>
#include <stdlib.h>
#include "holdem.h"

using namespace std;

int main() {

	holdem game(500, 52);
	game.playHoldEm();

	return 0;
}