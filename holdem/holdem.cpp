/*	Author: 		Reilly Kearney									*
 *	Date:			03/23/2015										*
 *	Description:	Hold 'em Implementation File for final project	*/

#include <iostream>
#include <iomanip>
#include <string>
#include <deque>
#include <algorithm>
#include <stdlib.h>
#include <unistd.h>
#include "holdem.h"
using namespace std;

holdem::holdem() {
}

holdem::holdem( double curr, double oppMoney ) {
	pot = 0;
	canBet = 1;
	
	/* Initialize Player */
	Player player;
	player.totalBet   = 0;
	player.currentBet = 0;
	player.totalMoney = curr;
	startMoney        = curr;
	currentMoney      = curr;

	/* Initialize Opponent */
	Player opponent;
	opponent.totalBet   = 0;
	opponent.currentBet = 0;
	opponent.totalMoney = oppMoney;

	initializeDeck();
}

void holdem::initializeDeck() {
	int rank = 0;
	for ( int j = 0; j < 13; j++ ) {
		for ( int i = 0; i < 4; i++ ) {
			Card card;
			card.suitRank  = i;
			card.valueRank = rank++;
			if ( j < 9 ) {
				card.value = to_string( j + 2 );
			}
			else {
				switch ( j ) {
					case 9:
						card.value = "J";
						break;
					case 10:
						card.value = "Q";
						break;
					case 11:
						card.value = "K";
						break;
					case 12:
						card.value = "A";
						break;
				}
			}
			switch ( i ) {
				case 0:		// clubs
					card.suit = "C";
					break;
				case 1:		// diamonds
					card.suit = "D";
					break;
				case 2:		// hearts
					card.suit = "H";
					break;
				case 3:		// spades
					card.suit = "S";
					break;
			}
		}
		deck.push_back( card );		// creates deck of Card structs
	}
	shuffle();
}

void holdem::shuffle() {
	random_shuffle( deck.begin(), deck.end() );
}

int holdem::getSize() {
	return deck.size();
}

void holdem::playHoldEm() {
	double bet;

	while ( 1 ) {
		betPrompt( 0 );
		TableStatus();

		/* Deal Initial Cards */
		deal();
		printHand( 0 );

		/* Pre-Flop */
		check = betting();

		/* Flop */
		if ( check ) {
			system( "clear" );
			flop();
			printHand( 0 );
			TableStatus();
			check = betting();
		}

		/* Turn */
		if ( check ) {
			system( "clear" );
			turn();
			printHand( 0 );
			TableStatus();
			check = betting();
		}

		/* River */
		if ( check ) {
			system( "clear" );
			river();
			printHand( 0 );
			TableStatus();
			check = betting();
		}

		/* Show Hands */
		system( "clear" );
		TableStatus();
		printTable( 5 );
		printHand( 0 );		// print player hand
		printHand( 1 );		// print opponent hand

		/* Check for Winner */
		switch (checkWinner()) {
			case 0:		// player loses
				opponent.totalMoney += pot;
				break;
			case 1:		// player wins
				player.totalMoney += pot;
				break;
		}

		/* Prompt for Continue Play */
		if ( !playAgain() ) { break; }
		else { reset(); }
	}
}

void holdem::deal() {
	player.hand   = new Card[ 2 ];
	opponent.hand = new Card[ 2 ];
	table         = new Card[ 5 ];

	for ( int i = 0; i < 2; i++ ) {
		player.hand[ i ] = deck.front();
		deck.pop_front();								// delete card dealt
	}
	for ( int i = 0; i < 2; i++ ) {
		opponent.hand[ i ] = deck.front();
		deck.pop_front();				// delete card dealt
	}
}

void holdem::flop() {
	for ( int i = 0; i < 3; i++ ) {
		table[ i ] = deck.front();
		deck.pop_front();			// delete card dealt
	}
	printTable( 3 );
}

void holdem::turn() {
	table[ 3 ] = deck.front();
	deck.pop_front();			// delete card dealt
	printTable( 4 );
}

void holdem::river() {
	table[ 4 ] = deck.front();
	deck.pop_front();			// delete card dealt
	printTable( 5 );
}

int holdem::betting() {
	int didFold;
	while ( 1 ) {
		/* Player Bets */
		didFold = betPrompt( 1 );
		if ( didFold ) { return 0; }	// return if fold
		newRound( 1 );					// reset opponenet current bet

		/* Opponent Bets */
		didFold = opponentPlaceBet();
		if ( didFold ) { return 0; }	// return if fold
		newRound( 0 );					// reset player current bet
	}
	return 1;		// game can continue
}

int holdem::betPrompt( int condition ) {
	int choice;
	int didBet = 0;
	switch ( condition ) {
		case 0:		// start
			cout << "Ante Up! Everyone places $20 in the pot." << endl;
			ante();
			break;
		case 1:		// no current round bet 
			cout << "\t1. Fold\t2.Check\t3.Raise\nWhat would you like to do? " << endl;
			cin >> choice;

			switch ( choice ) {
				case 1:		// Fold
					fold( 0 );
					return 1;
				case 2:		// Check
					check( 0 );
					canBet = 1;
					break;
				case 3:		// Raise
					raise( 0, 0 );
					canBet = 0;
					break;
			}
			break;
		case 2:		// bet already placed
			cout << "\t1. Fold\t2.Check\t3.Call\nWhat would you like to do? " << endl;
			cin >> choice;

			switch ( choice ) {
				case 1:		// Fold
					fold( 0 );
					return 1;
				case 2:		// Check
					check( 0 );
					canBet = 1;
					break;
				case 3:		// Call
					call( 0 );
					break;
			}
			break;

	}
	return 0;
}

void holdem::ante() {
	player.totalMoney   -= 20;
	player.totalBet     += 20;
	opponent.totalMoney -= 20;
	opponent.totalBet   += 20;
	pot                 += 40;
}

void holdem::fold( int x ) {
	switch ( x ) {
		case 0:		// player folds
			cout << "You fold" << endl;
			opponent.totalMoney += pot;
			break;
		case 1:		// opponent folds
			cout << "Opponent folds" << endl;
			player.totalMoney += pot;
			break;
	}
}

void holdem::check( int x ) {
	switch ( x ) {
		case 0:		// player checks
			cout << "You checked" << endl;
			break;
		case 1:		// opponent checks
			cout << "Opponent checked" << endl;
			break;
	}
}

void holdem::raise( int x, double bet ) {
	switch ( x ) {
		case 0:		// player raises
			if ( player.totalMoney == 0 ) {
				cout << "You are all in!"
				break;
			}
			else { 
				cout << "Place your bet: ";
				cin >> bet;
				player.currentBetv = bet;
				player.totalBet   += bet;
				player.totalMoney -= bet;
				pot               += bet;
			}
			break;
		case 1:		// opponent raises
			if ( opponent.totalMoney == 0 ) {
				cout << "Opponent is all in!" << endl;
				break;
			}
			opponent.currentBet  = bet;
			opponent.totalBet   += bet;
			opponent.totalMoney -= bet;
			pot                 += bet;
			break;
	}
}

void holdem::call( int x ) {
	switch ( x ) {
		case 0: 	// player calls
			cout << "You called" << endl;
			if ( player.totalMoney >= opponent.currentBet ) {
				player.currentBet  = opponent.currentBet;
			}
			else {
				player.currentBet = player.totalMoney;
				cout << "You are all in!" << endl;
			}
			player.totalBet   += player.currentBet;
			player.totalMoney -= player.currentBet;
			pot               += player.currentBet;
			break;
		case 1:		// opponent calls
			cout << "Opponent called" << endl;
			if ( opponent.totalMoney >= player.currentBet ) {
				opponent.currentBet  = player.currentBet;
			}
			else {
				opponent.currentBet  = opponent.totalMoney;
				cout << "Opponent is all in!" << endl;
			}
			opponent.totalBet   += opponent.currentBet;
			opponent.totalMoney -= opponent.currentBet;
			pot                 += opponent.currentBet;
			break;
	}
}

int holdem::playAgain() {
	char c;
	cout << "Your Current Money: " << player.totalMoney << endl;
	cout << "Would you like to play again? (Y/N) ";
	cin >> c;

	if      ( c == 'Y' || c == 'y' ) { return 1; }
	else if ( c == 'N' || c == 'n' ) { return 0; }
	return 0;
}

int holdem::checkWinner() {
	player.handScore = calculateHandScore( 0 );
	opponent.handScore = calculateHandScore( 1 );

	if ( player.handScore < opponent.handScore ) { return 0; }
  	else if ( player.handScore > opponent.handScore ) { return 1; }
  	else { return 0; }
}

void holdem::calculateHandScore( int x ) {
	// straight flush	- suits then rank
	table[ 0 ].suitRank == table[ 1 ].suitRank
	table[ 0 ].suitRank == table[ 2 ].suitRank
	table[ 0 ].suitRank == table[ 3 ].suitRank
	table[ 0 ].suitRank == table[ 4 ].suitRank
	table[ 1 ].suitRank == table[ 2 ].suitRank
	table[ 1 ].suitRank == table[ 3 ].suitRank
	table[ 1 ].suitRank == table[ 4 ].suitRank
	table[ 2 ].suitRank == table[ 3 ].suitRank
	table[ 2 ].suitRank == table[ 4 ].suitRank
	table[ 3 ].suitRank == table[ 4 ].suitRank

	// four of a kind	- add ranks then suit then high card
	int count0 = 0, count1 = 0;
	for ( int i = 0; i < 5; i++ ) {
		if ( player.hand[ 0 ].value == table[ i ].value ) {
			count0++;
		}
		if ( player.hand[ 1 ].value == table[ i ].value ){
			count1++;
		}
	}
	if ( player.hand[ 0 ].value == player.hand[ 1 ].value ) {
		count0++;
		count1++;
	}
	
	if ( count0 == 3 || count1 == 3 ) {
		// four of a kind found!
	}
	else if ( ( count0 == 2 && count1 == 1 ) || ( count0 == 1 && count1 == 2 ) ) {
		// full house found
	}
	else if ( count0 == 2 || count1 == 2 ) {
		// three of a kind found
	}
	else if ( count0 == 1 && count1 == 1 ) {
		// two pair
	}
	else if ( count0 == 1 || count1 == 1 ) {

	}

	// full house		- add ranks of cards then suits

	// flush			- add suits then rank

	// straight			- add ranks then suit

	// three of a kind	- add ranks then suit then high card

	// two pair			- add rnaks then suit then high card

	// pair				- add ranks then suit then high card

	// high card 		- rank then suit
		Card high;
		high = player.hand[ 0 ];
		
		if ( (high.valueRank < player.hand[ 1 ].valueRank) || (high.valueRank == player.hand[ 1 ].valueRank && high.suitRank < player.hand[ 1 ].suitRank) ) { high = player.hand[ 1 ]; }
		for ( int i = 0; i < 5; i++ ) {
			if ( high.valueRank < table[ i ].valueRank || (high.valueRank == table[ i ].valueRank && high.suitRank < table[ i ].suitRank) ) { high = table.[ i ]; }
		}

	
	/* switch ( x ) {
		case 0:		// player
			break;
		case 1:		// opponent
			break;
	} */
}

void holdem::reset() {
	initializeDeck();
	delete[] playerHand;
	delete[] opponentHand;
	delete[] table;
	player.totalBet   = 0;
	opponent.totalBet = 0;
	pot               = 0;
	newRound();
}

void holdem::newRound( int x ) {
	if ( !x ) {
		player.currentBet = 0;
	}
	else if ( x ) {
		opponent.currentBet = 0;
	}
}

void holdem::printHand( int n ) {
	switch ( n ) {
		case 0:		// print just player hand
			cout << "Your Hand:"           << endl;
			cout << player.hand[ 0 ].suit  << "  " << player.hand[ 1 ].suit  << endl;
			cout << player.hand[ 0 ].value << "  " << player.hand[ 1 ].value << endl;
			break;
		case 1:		// print opponent hand
			cout << "Opponent Hand:"        << endl;
			cout << opponent.hand[ 0 ].suit  << "  " << opponent.hand[ 1 ].suit  << endl;
			cout << opponent.hand[ 0 ].value << "  " << opponent.hand[ 1 ].value << endl;
			break;
	}
}

void holdem::printTable( int n ) {
	cout << endl;
	cout << "Table:\t";
	for ( int i = 0; i < n; i++ ) {
		cout << table[ i ].suit << "  ";
	}
	cout << endl;
	for ( int i = 0; i < n; i++ ) {
		cout << table[ i ].value << "  ";
	}
	cout << endl;
}

void holdem::TableStatus() {
	cout << "Your Current Money: "     << player.totalMoney        << "\t||\tYour Current Bet: "     << player.currentBet   << endl;
	cout << "Opponent Current Money: " << opponent.totalMoney << "\t||\tOpponent Current Bet: " << opponent.currentBet << endl;
	cout << "Total Money in the Pot: " << pot                 << endl;
}

int holdem::opponentPlaceBet() {
	int action = determineAction();
	double bet = 0;

	switch ( action ) {
		case 0:		// fold
			fold( 1 );
			return 1;
		case 1:		// check
			check( 1 );
			break;
		case 2:		// raise
			bet = determineBet();
			raise( 1, bet );
			break;
		case 3:		// call
			call( 1 );
			break;
	}
	return 0;
}

int holdem::determineAction() {
	/* Conditions to determine how to bet */
	if ( opponent.hand[ 0 ].suitRank == opponent.hand[ 1 ].suitRank ) {
		return 1;
	}
	else if ( opponent.hand[ 0 ].valueRank < 8 && opponent.hand[ 1 ].valueRank < 8 ) {
		if ( opponent.hand[ 0 ].valueRank != opponent.hand[ 1 ].valueRank ) { return 0; }	// fold
		else { return 1; }	// check
	}
	else if ( opponent.hand[ 0 ].valueRank < 10 && opponent.valueRank[ 1 ].rank < 10 ) {
		if ( opponent.hand[ 0 ].valueRank != opponent.hand[ 1 ].valueRank ) { return 1; } 
		else { return 2; }
	}
	return 2;
}

void holdem::determineBet( int action ) {
	double bet;
	/* Conditions to check for magnitude of bet */
	if ( !action ) {
		fold( 1 );
	}
	else if ( action == 1 && !canBet ) {
		check( 1 )
	}
	else if ( action && canBet ) {
		// determine amount for bet

		raise( 1, bet ); 
	}
}

void holdem::EndOfGame() {

}

double holdem::netChange() {
	return ( player.totalMoney - startMoney );
}