// slots.h (class definition)
// Due 2/8/16
// Author: Ronni Sardina
// Derived class of casino
//IN ORDER TO COMPILE, YOU HAVE TO MAKE WITH CASINO.CPP THEN MAKE WITHOUT CASINO.CPP

#ifndef slots_H
#define slots_H
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <time.h>
#include <set>
#include <map>
#include "math.h"
#include "casino.h"
//#include "casino.cpp"
using namespace std;

// Derived slots class; inherits from casino
class slots : public casino {
	public:
		slots();	// default constructor for slots class
		void playGame();	///should be changed later to return the gameMoney
		void print();
	private:
		char spin();
		int compare();
		
		char wheel1;
		char wheel2;
		char wheel3;
		char wheel4;
		int matches;
		//char images[9] = {'☘', '☂', '☮', '✱', '✈', '♥', '♦', '♠', '♣'};
		char images[9] = {'X', 'O', '#', '%', '$', '@', '*', '?', '+'};
		map<char, int> iconCount;
		int play;
};

#endif
