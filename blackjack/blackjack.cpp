 /*	Author: 		Reilly Kearney											*
 *	Date:			03/23/2015												*
 *	Description:	Black Jack Implementation File for final project		*/

#include <iostream>
#include <iomanip>
#include <string>
#include <deque>
#include <algorithm>
#include <stdlib.h>
#include "blackjack.h"
using namespace std;

blackjack::blackjack() {
}

blackjack::blackjack( double curr, int n ) {
	startMoney = curr;
	currentMoney = curr;
	currentBet = 0;

	for (int i = 0; i < n; i++) {
		deck.push_back(i % 13);		// creates deck with values from 0 - 12
	}
	shuffle();
}

void blackjack::shuffle() {
	random_shuffle(deck.begin(), deck.end());
}

int blackjack::getSize() {
	return deck.size();
}

void blackjack::playBlackJack() {
	int dealerWins = 0, playerWins = 0, push = 0;	// total wins for both dealer & player
	int playOn = 1;									// 1 for play game, 0 for stop
	int continueHand = 1;							// 1 for keep asking player, 0 for move onto dealer	
	int playerHand = 0, dealerHand = 0;				// Hand for both player & dealer
	int check, i, winner;
	double bet;

	while (playOn) {
		/* Print Money */
		cout << "Your Current Money: " << currentMoney << endl;

		/* Place Bet */
		cout << "Place your bet: ";
		cin >> bet;
		if (!placeBet(bet)) { continue; }

		/* Deal Initial Cards */
		playerHand = Deal('P');
		dealerHand = Deal('D');
		
		/* Check for Player BlackJack */
		if (check21(playerHand, 'P')) {
			continueHand = 0;
		}

		/* Player Turn */
		while (continueHand) { 
			check = checkHit();
			if (check) {
				playerHand += checkCardValue(deck.front());
				cout << "You Received: ";
				printCard(deck.front());
				deck.pop_front();
			}
			if ((playerHand > 21) || (!check)) {
				continueHand = 0;
			}
			cout << "Current Hand: " << playerHand << endl;
		}
		
		cout << endl;
		
		/* Check for Dealer BlackJack */
		check21(dealerHand, 'D');
		
		cout << "Dealer has " << dealerHand << endl;

		/* Dealer Turn */
		while (dealerHand < 17) {
			dealerHand += checkCardValue(deck.front());
			cout << "Dealer received ";
			printCard(deck.front());
			deck.pop_front();
			cout << "Dealer now has " << dealerHand << endl;
		}

		/* Check for Winner */
		winner = checkWinner(playerHand, dealerHand);
		switch (winner) {
			case 0:	// player loses
				dealerWins += 1;
				break;
			case 1:	// player wins
				playerWins += 1;
				break;
			case 2:	// push
				push += 1;
				break;
		}
	
		/* Print Money */
		cout << "Your Current Money: " << currentMoney << endl;

		/* Prompt for Continue Play */
		playOn = playAgain();

		/* Check Deck Size */
		if (getSize() <= 15) {
			deck.erase(deck.begin(), deck.end());
			for (i = 0; i < 52; i++) {
				deck.push_back(i % 13);
			}
			shuffle();
		}

		/* Reset GamePlay */
		playerHand = 0;
		dealerHand = 0;
		continueHand = 1;
	}
	
	/* Display Stats */
	cout << endl;
	cout << "Player Won " << playerWins << " Games." << endl;
	cout << "Dealer Won " << dealerWins << " Games." << endl;
	cout << "Pushed " << push << " Games." << endl;
}

int blackjack::placeBet( double bet ) {
	currentBet = bet;
	if (currentBet > currentMoney) { return 0; }
	else {
		currentMoney = currentMoney - currentBet;
		return 1;
	}
}

void blackjack::Payout( int n ) {
	switch (n) {
		case 0:
			break;
		case 1: // user won
			currentMoney = currentMoney + (2 * currentBet);	// paid out
			break;
		case 2: // pushed
			currentMoney = currentMoney + currentBet;		// break even
			break;
		case 3: // blackjack
			currentMoney = currentMoney + (4 * currentBet);	// paid out double
	}
}

int blackjack::Deal(char z) {
	int hand = 0, i;
	if (z == 'P') {			// deals to player
		cout << "Your Hand: " << endl;
		for (i = 0; i < 2; ++i) {
			hand += checkCardValue(deck.front());;
			printCard(deck.front());		// prints for user to know cards
			deck.pop_front();				// delete card dealt
		}
	}
	else if (z == 'D') {	// deals to dealer
		for (i = 0; i < 2; i++) {
			hand += checkCardValue(deck.front());;
			deck.pop_front();				// delete card dealt
		}
	}
	return hand;
}

int blackjack::playAgain() {
	char c;
	cout << "Would you like to play again? (Y/N) ";
	cin >> c;

	if (c == 'Y' || c == 'y') {
		return 1;
	}
	else if (c == 'N' || c == 'n') {
		return 0;
	}

	return 0;
}

int blackjack::checkWinner (int playerHand, int dealerHand) {
	if (playerHand <= 21) {
		if (dealerHand > 21) {
			cout << "Congrats! Dealer Bust! You Win!" << endl;
			Payout(1);
			return 1;
		}
		else if (playerHand > dealerHand) {
			cout << "Congrats! You Win!" << endl;
			Payout(1);
			return 1;
		}
		else if (playerHand < dealerHand) {
			cout << "Dealer Wins! Better Luck next time!" << endl;
			Payout(0);
			return 0;
		}
		else if (playerHand == dealerHand) {
			cout << "You Pushed!" << endl;
			Payout(2);
			return 2;
		}
	}
	else if (playerHand > 21) {
		cout << "You Bust! Dealer Wins! Better Luck next time!" << endl;
		return 0;
	}

	return 0;
}

int blackjack::checkHit() {
	char c;
	cout << "Would you like to hit? (Y/N) ";
	cin >> c;
	if ( c == 'Y' || c == 'y' ) {
		return 1;
	}
	else if ( c == 'N' || c == 'n' ) {
		return 0;
	}
	else {
		return 0;
	}
}

int blackjack::checkCardValue(int v) {
	int output;
	switch (v) {
		case 1:		// Ace
			output = 11;
			break;
		case 11:	// Jack
		case 12:	// Queen
		case 0:		// King
			output = 10;
			break;
		default:
			output = v;
	}
	return output;
}

void blackjack::printCard(int v) {
	switch (v) {
		case 1:
			cout << "Ace " << endl;
			break;
		case 11:
			cout << "Jack " << endl;
			break;
		case 12:
			cout << "Queen " << endl;
			break;
		case 0:
			cout << "King " << endl;
			break;
		default:
			cout << v << " " << endl;
	}
}

int blackjack::check21(int hand, char z) {
	if (hand == 21 && z == 'P') {
		cout << "YOU GOT BLACKJACK!" << endl;
		Payout(3);
		return 1;
	}
	else if (hand == 21 && z == 'D') {
		cout << "DEALER GOT BLACKJACK!" << endl;
		Payout(0);
		return 1;
	}
	else {
		return 0;
	}
}

double blackjack::netChange() {
	double netChange;
	netChange = currentMoney - startMoney;
	return netChange;
}