/*	Author: 		Reilly Kearney									*
 *	Date:			03/23/2015										*
 *	Description:	Black Jack Interface File for final project		*/

#ifndef blackjack_h
#define blackjack_h

#include <iostream>
#include <string>
#include <deque>
#include <iomanip>
using namespace std;

class blackjack {
	
	public:
		blackjack(); 						// default constructor for blackjack class
		blackjack( double, int = 52 );
		void shuffle();						// shuffles cards
		int getSize();						// returns size of deck
		void playBlackJack();				// BlackJack Interface
		int placeBet(double);				// place a bet at beginning of each game
		void Payout(int);					// payout if win or push
	private:
		int Deal(char);						// deals cards
		int playAgain();					// asks user if still playing
		int checkWinner(int, int);			// determines winner (0 dealer, 1 player, -1 push) & prints appropriate message
		int checkHit();						// returns 0 if stand, 1 if hit
		int checkCardValue(int);			// adjust card value for ace, king, queen, and jack
		void printCard(int);				// prints card
		int check21(int, char);				// checks for BlackJack
		double netChange();					// returns net money lost or gained
		deque< int > deck;
		double currentBet;					// monetary value for bet
		double startMoney;					// different from initialMoney
		/* Information from base class - will adjust in future to link*/
		double currentMoney;
};

#endif 