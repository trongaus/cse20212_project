// keno.cpp

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <string>
#include "keno.h"

// non-default constructor to initialize random array
keno::keno(double curr){
	startMoney = curr;
	currentMoney = curr;
}

void keno::kenoArray(){
	srand((unsigned)time(0));
	for (int i = 0; i < 20; i++){
		gameNumbers[i] = (rand()%80)+1;
	}
}

// ask user for 10 different numbers
void keno::playerChoice(){
	cout << "Please enter your 10 wagered numbers (between 1-80)" << endl;
	int n;
	int maxNums = 10;
	for (int i = 0; i < maxNums; i++){
		if (cin >> n){
			while (n < 1 || n > 80){
				cout << "Invalid number. Try again." << endl;
				cin >> n;
			}
			userNumbers[i] = n;
		}
	}
}

// compare userNumbers to gameNumbers
int keno::checkChoice(){	
	int match = 0;
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 20; j++) {
			if (userNumbers[i] == gameNumbers[j]){
				match +=1;
			}
		}
	}
	cout << "Number of matches: " << match << endl;
	return match;	
}

void keno::payout(int n){
// note: currently arbitrary values
	switch(n) {
		case 0:
			currentMoney = currentMoney + currentBet;
		case 1-5:
			currentMoney = currentMoney + (1.5 * currentBet);	
		case 6-9:
			currentMoney = currentMoney + (2 * currentBet);
		case 10: 
			currentMoney = currentMoney + (4 * currentBet);
	}
}

void keno::playKeno(){
	int playGame = 1, numMatch = 0;
	double bet = 0;
	
	while (playGame == 1){	
		cout << "Your current amount of money: " << currentMoney << endl;
		cout << "Place your bet: ";
		cin >> bet;
		
		while (bet > currentMoney){
			cout << "Bet too large! Try again!" << endl;
			cout << "Place your bet: ";
			cin >> bet;
		}
		
		currentMoney = currentMoney - bet;

		kenoArray();
		playerChoice();
		cout << "---Game Numbers---" << endl; 
		for (int i = 0; i < 20; i++){
			if (i < 19){
				cout << gameNumbers[i] << ", ";
			}
			else {
				cout << gameNumbers[i] << endl;
			}
		}
		numMatch = checkChoice();	
		payout(numMatch);
		playGame = playAgain();
	}
}

int keno::playAgain() {
	char c = 'Z';
	if (c != 'Y' || c != 'N'){
		cout << "Would you like to play again? (Y/N) "; 
		cin >> c;
	} 
	if (c == 'Y') {
		return 1;
	}
	else {
		return 0;
	}
}
